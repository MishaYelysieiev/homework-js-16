const changeThemeBtn = document.createElement("button");

changeThemeBtn.classList.add("btn");
changeThemeBtn.innerText = "Change theme";
changeThemeBtn.style.backgroundColor="red";
changeThemeBtn.style.marginTop="30px";

document.getElementsByClassName("background")[0].appendChild(changeThemeBtn);

checkThemeStatus = () => {
    let status = localStorage.getItem("themeState");
    localStorage.setItem(status,'true');
    const link = document.createElement("link");
    document.head.appendChild(link);  
    link.rel = "stylesheet";
    if(status==='true'){
        link.href="style/dark-style.css";
    } else{
        link.href="style/light-style.css";
    }
};

checkThemeStatus();

changeThemeBtn.addEventListener("click",()=>{
    let status = localStorage.getItem("themeState");
    localStorage.setItem(status,'true');
    if(status==='true'){
        localStorage.setItem("themeState","false");
    }else{
        localStorage.setItem("themeState","true");
    };
    checkThemeStatus();
});







